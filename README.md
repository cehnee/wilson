# Wilson

A RESTful API for managing relationships between denizens of a deserted island.

# Installation

```
npm install
npm start
```

## API Endpoints

### (**POST**) /api/Organizations/createWithRelations 
Expects JSON data with organizations tree in body.

Saves given organizations to database. 

Request example:
```
/api/Organizations/createWithRelations
```
Request body example:
```javascript
[
    {
        "org_name": "Paradise Island",
        "daughters": [
            {
                "org_name": "Banana tree",
                "daughters": [
                    {"org_name": "Yellow Banana"},
                    {"org_name": "Brown Banana"},
                    {"org_name": "Black Banana"}
                ]
            }   
        ]
    },
    {
        "org_name": "Big Banana tree",
        "daughters": [
            {"org_name": "Yellow Banana"},
            {"org_name": "Brown Banana"},
            {"org_name": "Green Banana"},
            {
                "org_name": "Black Banana",
                "daughters": [
                    {
                        "org_name":"Phoneutria Spider"
                    }   
                ]
            }
        ]
    }
]
```

### (**GET**) /api/Organizations/findRelations
Expects query parameter named `name` (mandatory).

Supports pagination via query parameters `filter[offset]` and `filter[limit]`.

Returns first class relatives: parents, daughters and sisters of given organization. 

Request example:
```
/api/Organizations/findRelations?name=Black%20Banana&filter[offset]=2&filter[limit]=3
```

Response example:
```js
[
    {
      "type": "sister",
      "org_name": "Brown Banana"
    },
    {
      "type": "sister",
      "org_name": "Green Banana"
    },
    {
      "type": "daughter",
      "org_name": "Phoneutria Spider"
    }
  ]
```
