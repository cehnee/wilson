'use strict';

const _ = require('underscore');
const async = require('async');

module.exports = (Organization) => {
  // returns first order relatives for the organization whose name matches input
  // case study in promises
  Organization.findRelations = function(name, filter, cb) {
    let Relationship = Organization.app.models.Relationship;

    Organization.findOne({where: {'org_name': name}})
      .then(org => {
        console.log('Organization found: ' + org.id);
        return findRelatives(org, filter);
      })
      .then(relatives => {
        cb(null, relatives);
      })
      .catch(function(err) {
        console.log(err.message);
        cb(err);
      });

    const findRelatives = function(org, filter) {
      let promise = new Promise(function(resolve, reject) {
        if (null == org) {
          let error = new Error('Organization not found.');
          error.status = 404;
          reject(error);
        }

        // TODO: move this hardcode to config or better yet, replace with proper loopback
        let offset = 0;
        let limit = 100;
        if (!_.isUndefined(filter)) {
          console.log(filter);
          if (!isNaN(+(filter.limit)) && (+(filter.limit) < 100)) {
            limit = +(filter.limit);
          }
          if (!isNaN(+(filter.offset))) {
            offset = +(filter.offset);
          }
        }
        console.log('Offset,limit: ' + offset + ',' + limit);

        const connector = Organization.app.dataSources.mydb.connector;
        let params = [org.id, org.id, org.id, offset, limit];
        let stmt = `
           (SELECT 'parent' AS relationship_type, p.org_name AS org_name
            FROM Relationship r
              INNER JOIN Organization p ON r.parent_id = p.id
            WHERE r.daughter_id = ?)
          UNION
           (SELECT 'daughter' AS relationship_type, d.org_name AS org_name
            FROM Relationship r
              INNER JOIN Organization d ON r.daughter_id = d.id
            WHERE r.parent_id = ?)
          UNION
           (SELECT 'sister' AS relationship_type, s.org_name AS org_name
            FROM Relationship pr
              INNER JOIN Relationship sr ON pr.parent_id = sr.parent_id
              INNER JOIN Organization s ON sr.daughter_id = s.id
            WHERE pr.daughter_id = ? AND sr.daughter_id != pr.daughter_id)
          ORDER BY org_name
          LIMIT ?,?
        `;
        connector.execute(stmt, params, (error, results) => {
          if (error) reject(error);
          resolve(results);
        });
      });
      return promise;
    };
  };

  // batch creates organizations from the input along with relationships between them
  // case study in async and callbacks
  Organization.createWithRelations = function(orgs, cb) {
    let Relationship = Organization.app.models.Relationship;

    let flattenedOrgs = [];
    const flattenOrganizations = function(obj, parent) {
      let parentForDaughters = parent;
      if (Array.isArray(obj)) {
        // traverse array of organizations
        for (let key in obj) {
          flattenOrganizations(obj[key], parent);
        }
      } else {
        // parse organization object
        for (let key in obj) {
          if (key == 'org_name') {
            parentForDaughters = obj[key];
            let org = {
              name: obj[key],
              daughterId: undefined,
              parentName: parent,
              parentId: undefined,
            };
            flattenedOrgs.push(org);
          } else if (key == 'daughters') {
            flattenOrganizations(obj[key], parentForDaughters);
          }
        }
      }
    };
    flattenOrganizations(orgs);
    console.log(JSON.stringify(flattenedOrgs));

    let foundOrCreatedOrgs = {};
    async.eachSeries(flattenedOrgs, function(item, callback) {
      // attempt to find or create every organization from the input
      let filter = {'org_name': item.name};
      Organization.findOrCreate({where: filter}, filter, function(err, org) {
        if (err) return;
        console.log('+ ' + JSON.stringify(org));
        foundOrCreatedOrgs[item.name] = org.id;
        // attempt to find or create a relationship between the organization and its parent
        let parentId = foundOrCreatedOrgs[item.parentName];
        let filter = {'daughter_id': org.id, 'parent_id': parentId};
        Relationship.findOrCreate({where: filter}, filter, function(err, rel) {
          if (err) { throw err; return; }
          console.log('++ ' + JSON.stringify(rel));
        });
        callback(null, org);
      });
    }, function(err) {
      if (err) throw err;
      else console.log('Import complete.');
    });

    return cb(null, 'Organizations successfully imported.');
  };

  Organization.remoteMethod('findRelations',
    {
      http: {path: '/findRelations', verb: 'get', status: 200},
      accepts: [
        {arg: 'name', type: 'string', required: true, http: {source: 'query'}},
        {arg: 'filter', type: 'object', http: {source: 'query'}},
      ],
      returns: {arg: 'relations', type: 'array', root: true},
    }
  );
  Organization.remoteMethod('createWithRelations',
    {
      http: {path: '/createWithRelations', verb: 'post', status: 201},
      accepts: {arg: 'orgs', type: 'object', http: {source: 'body'}},
      returns: {arg: 'result', type: 'string', root: true},
    }
  );
};
